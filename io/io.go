package io

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path"
	"strings"
)

type ZeroString string

func openUrl(url string) ([]byte, error) {
	var res *http.Response
	var err error
	for i := 0; i < 5; i++ {
		res, err = http.Get(url)
		if err == nil {
			break
		}
	}
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	return data, nil
}

func OpenHTML(path string) ([]byte, error) {
	htmlContent, err := openUrl(path)
	htmlContent = []byte(strings.Replace(
		string(htmlContent), "(>_<)", "(&gt;_&lt;)", -1))
	return htmlContent, err
}

func Exists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}

func Path(url string, dst string, num ZeroString) string {
	return dst + "/" + string(num) + path.Ext(string(url))
}

func Save(url string, dst string, num ZeroString) error {
	filepath := Path(string(url), dst, num)
	isExist, err := Exists(filepath)
	if isExist {
		return nil
	}

	data, err := openUrl(string(url))
	if err != nil {
		fmt.Println("retry", string(url))
		data, err = openUrl(string(url))
		if err != nil {
			return err
		}
	}

	os.MkdirAll(dst, 0755)
	file, err := os.Create(filepath)
	if err != nil {
		return err
	}

	_, err = file.Write(data)
	if err != nil {
		file.Close()
		os.Remove(filepath)
		return err
	}
	file.Close()
	return nil
}

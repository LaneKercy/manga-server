package parser

import (
	"fmt"
	"testing"
	"time"
)

func TestChapters(t *testing.T) {
	mangasUrl := "http://www.mangareader.net"
	var wantStr string

	mangaParser := NewMangaParser(mangasUrl)
	mangas := mangaParser.GetMangasList()

	manga, err := mangaParser.GetUrlSuffix(mangas[0])
	if err != nil {
		t.Errorf("%v", err.Error())
	}

	fmt.Print(manga)
	chapterParser := NewChapterParser(manga, mangas[0])

	// chapter list not yet initialized
	date, err := chapterParser.GetDate(ChapterIndex(0))
	if err == nil {
		t.Error("GetDate(\"any\") => err must not be nil")
	}

	url, err := chapterParser.GetUrl(ChapterIndex(0))
	if err == nil {
		t.Error("GetUrl(\"any\") => err must not be nil")
	}

	// init chapter list
	chapters, err := chapterParser.GetChaptersAndShortUrls()
	if err != nil || len(chapters) == 0 {
		t.Errorf("%v", err.Error())
	}

	date, err = chapterParser.GetDate(ChapterIndex(1))
	if err != nil {
		t.Error("GetDate(chapter) => err should be nil")
	}
	wantTime := time.Date(2012, time.Month(11), 13, 0, 0, 0, 0, time.UTC)
	if date != wantTime {
		t.Errorf("date is %v, want %v\n",
			date.String(), wantTime.String())
	}

	url, err = chapterParser.GetUrl(ChapterIndex(1))
	if err != nil {
		t.Error("GetDate(chapter) => err should be nil")
	}
	wantStr = "/junjou-drop/1"
	name, _ := chapterParser.GetName(ChapterIndex(1))
	fmt.Println("name", name)
	if string(url) != wantStr {
		t.Errorf("url is %v, want %v\n", string(url), wantStr)
	}
}

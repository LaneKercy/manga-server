package parser

import (
	"fmt"
	"testing"
)

func TestCovers(t *testing.T) {
	mangasUrl := "http://www.mangareader.net"
	var wantStr string

	mangaParser := NewMangaParser(mangasUrl)
	mangas := mangaParser.GetMangasList()

	manga, err := mangaParser.GetUrlSuffix(mangas[0])
	if err != nil {
		t.Errorf("%v", err.Error())
	}

	fmt.Print(manga)
	coverParser := NewCoverParser(manga)

	coverUrl, err := coverParser.GetCoverUrl()
	if err != nil || len(coverUrl) == 0 {
		t.Errorf("%v", err.Error())
	}

	wantStr = "http://s3.mangareader.net/cover/junjou-drop/junjou-drop-l0.jpg"
	if string(coverUrl) != wantStr {
		t.Errorf("url is %v, want %v\n", string(coverUrl), wantStr)
	}
}

package parser

import (
	mangaio "bitbucket.org/mtrath/manga-server/io"
	"bytes"
	"encoding/xml"
	"errors"
	"sort"
	"strings"
)

type div struct {
	Lists []ul `xml:"div>ul"`
}

type ul struct {
	ListItem []li `xml:"li"`
}

type li struct {
	Hyperlink ahref `xml:"a"`
}

type ahref struct {
	Url  string `xml:"href,attr"`
	Name string `xml:",chardata"`
}

type Url string

type MangaUrl string

type MangaParser struct {
	url       Url
	mangas    map[string]MangaUrl
	mangaList []string
}

func NewMangaParser(url string) *MangaParser {
	parser := new(MangaParser)
	parser.url = Url(url)

	return parser
}

func (this *MangaParser) openUrl(site Url) (*bytes.Reader, error) {
	path := string(this.url + site)
	htmlContent, err := mangaio.OpenHTML(path)
	return bytes.NewReader(htmlContent), err
}

func (this *MangaParser) GetMangasAndShortUrls() (map[string]MangaUrl, error) {
	if len(this.mangas) > 0 {
		return this.mangas, nil
	}
	this.mangas = make(map[string]MangaUrl)
	htmlFile, err := this.openUrl("/alphabetical")
	if err != nil {
		return this.mangas, nil
	}

	parser := xml.NewDecoder(htmlFile)
	parser.Strict = false

	var countDivs = 0
	for {
		if countDivs == 2 {
			break
		}
		token, err := parser.Token()
		if err != nil {
			break
		}

		if startElement, ok := token.(xml.StartElement); ok &&
			startElement.Name.Local == "div" &&
			len(startElement.Attr) == 1 &&
			startElement.Attr[0].Value == "series_col" {
			var lists div
			parser.DecodeElement(&lists, &startElement)
			for _, list := range lists.Lists {
				for _, item := range list.ListItem {
					name := item.Hyperlink.Name
					if !strings.Contains(name, "\"") {
						url := MangaUrl(item.Hyperlink.Url)
						this.mangas[name] = url
					}
				}
			}
			countDivs++
		}
	}

	this.mangaList = make([]string, 0)
	return this.mangas, nil
}

func (this *MangaParser) GetMangasList() []string {
	if len(this.mangas) == 0 {
		this.GetMangasAndShortUrls()
	}
	if len(this.mangaList) > 0 {
		return this.mangaList
	}

	mapKeys := make([]string, len(this.mangas))
	i := 0
	for name, _ := range this.mangas {
		mapKeys[i] = name
		i++
	}
	sort.Strings(mapKeys)
	return mapKeys
}

func (this *MangaParser) GetUrlSuffix(name string) (MangaUrl, error) {
	if len(this.mangas) == 0 {
		return "", errors.New("The manga list may not have been " +
			"initialized. Use a GetMangas*() function first.")
	}
	url, ok := this.mangas[name]
	if ok {
		return url, nil
	}
	return "", errors.New("Manga " + name + " not found in map.")
}

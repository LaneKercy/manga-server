package parser

import (
	"testing"
)

func TestMangas(t *testing.T) {
	mangaUrl := "http://www.mangareader.net"
	var wantStr string

	mangaParser1 := NewMangaParser(mangaUrl)
	mangas1 := mangaParser1.GetMangasList()
	wantStr = "len > 3000"
	if len(mangas1) < 3000 {
		t.Errorf("len(mangas1) is %v, want %v\n", len(mangas1), wantStr)
	}

	mangaParser2 := NewMangaParser(mangaUrl)
	mangas2, err := mangaParser2.GetMangasAndShortUrls()
	if err != nil || len(mangas2) < 3000 {
		t.Errorf("len(mangas2) is %v, want %v\n", len(mangas2), wantStr)
	}

	if len(mangas1) != len(mangas2) {
		t.Errorf("mangas1 != mangas2, should be equal.\nmangas1: %v, mangas2: %v", len(mangas1), len(mangas2))
	}

	_, err = mangaParser1.GetUrlSuffix("BleachNaruto")
	if err == nil {
		t.Error("GetUrlSuffix(\"BleachNaruto\") => err must not be nil")
	}

	wantStr = "/94/bleach.html"
	manga, err := mangaParser1.GetUrlSuffix("Bleach")
	if err != nil {
		t.Error("GetUrlSuffix(\"BleachNaruto\") => err should be nil")
	}
	if manga != "/94/bleach.html" {
		t.Errorf("url is %v, want %v\n", manga, wantStr)
	}

	_, err = mangaParser2.GetUrlSuffix("BleachNaruto")
	if err == nil {
		t.Error("GetUrlSuffix(\"BleachNaruto\") => err must not be nil")
	}

	wantStr = "/94/bleach.html"
	manga, err = mangaParser2.GetUrlSuffix("Bleach")
	if err != nil {
		t.Error("GetUrlSuffix(\"BleachNaruto\") => err should be nil")
	}
	if manga != "/94/bleach.html" {
		t.Errorf("url is %v, want %v\n", manga, wantStr)
	}
}

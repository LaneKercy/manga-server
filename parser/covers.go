package parser

import (
	mangaio "bitbucket.org/mtrath/manga-server/io"
	"bytes"
	"encoding/xml"
)

type divCoverHolder struct {
	Img cover `xml:"img"`
}

type cover struct {
	Url string `xml:"src,attr"`
}

type CoverUrl string

type Cover struct {
	Name string
	Url  CoverUrl
}

type CoverParser struct {
	BaseUrl     Url
	MangaUrl    MangaUrl
	oldMangaUrl MangaUrl
	coverUrl    CoverUrl
}

func NewCoverParser(mangaUrlSuffix MangaUrl) *CoverParser {
	parser := new(CoverParser)
	parser.BaseUrl = "http://www.mangareader.net"
	parser.MangaUrl = mangaUrlSuffix
	return parser
}

func (this *CoverParser) openUrl() (*bytes.Reader, error) {
	path := string(this.BaseUrl) + string(this.MangaUrl)
	htmlContent, err := mangaio.OpenHTML(path)
	return bytes.NewReader(htmlContent), err
}

func (this *CoverParser) GetCoverUrl() (CoverUrl, error) {
	if this.oldMangaUrl == this.MangaUrl {
		return this.coverUrl, nil
	}
	htmlFile, err := this.openUrl()
	if err != nil {
		return "", nil
	}
	this.oldMangaUrl = this.MangaUrl

	parser := xml.NewDecoder(htmlFile)
	parser.Strict = false
	parser.AutoClose = xml.HTMLAutoClose
	parser.Entity = xml.HTMLEntity

	var imgXml xml.StartElement
	for {
		token, err := parser.Token()
		if err != nil {
			return this.coverUrl, nil
		}

		if startElement, ok := token.(xml.StartElement); ok &&
			startElement.Name.Local == "div" &&
			len(startElement.Attr) == 1 &&
			startElement.Attr[0].Value == "mangaimg" {
			imgXml = startElement
			break
		}
	}

	var div divCoverHolder
	parser.DecodeElement(&div, &imgXml)
	this.coverUrl = CoverUrl(div.Img.Url)

	return this.coverUrl, nil
}

// Main package. It contains the main function that initializes the server.
package main

import (
	"archive/zip"
	mangaio "bitbucket.org/mtrath/manga-server/io"
	"bitbucket.org/mtrath/manga-server/parser"
	"bytes"
	"compress/gzip"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

// mangas contains the names and url-paths for all mangas found. mangaList
// contains the names of all mangas. mangaList is empty by default and will
// only be filled if the getMangaList() function is called. If a new manga list
// is retrieved mangaList is reinitialized with len() = 0.
// lastCheck is used to check if it's necessary to retrieve a new manga list.
// baseFolder will be set via the commandline option "dst" and represents
// the destination directory.
var (
	logger      *log.Logger         = log.New(os.Stdout, "", log.LstdFlags|log.Lshortfile)
	mangaParser *parser.MangaParser = parser.NewMangaParser(mangaUrl)
	mangas      map[string]parser.MangaUrl
	mangaList   []string
	lastCheck   time.Time = time.Unix(0, 0)
	baseFolder  string
)

// These flags can be set via commandline options.
// Default port to listen to is 8080.
// dst is the destination directory where the server will save all images and
// zip files. Default is "archive". The directory "archive" will be created if
// it does not exist. If another path is set, the directory will NOT being
// created. The user has to make sure that the directory already exist.
var (
	port, dst string
)

// The url constants are the url-paths which will be handled by the handler
// functions. recheckDuration represents a period. If it has lapsed, the
// current mangaList will be considered as too old and a new one is retrieved.
const (
	dateFormat      string        = "02.01.2006"
	debugUrl        string        = "/manga/debug"
	listUrl         string        = "/manga/list"
	chapterUrl      string        = "/manga/ch/"
	coverUrl        string        = "/manga/cover/"
	getUrl          string        = "/manga/get/"
	mangaUrl        string        = "http://www.mangareader.net"
	recheckDuration time.Duration = time.Hour * 24 * 7
	defaultDst      string        = "archive"
)

// request is a container for a new http request that wants to retrieve a zip
// file, i.e. if a "/manga/get/" url is invoked, then the handler function which
// processes this request is creating a new request and send it to the monitor
// via a channel. The monitor is supervising all zip file requests and makes
// sure, that no race conditions or other strange effects can occur.
//
// W is the ResponseWriter where the server response is written to.
// R contains the actual http request. It contains, among other things, the
// requested url.
// Path is the chapter specific directory where images are downloaded to, and
// where the zip file will be generated.
// It may look like "archive/Bleach/00002".
// Manga is the name of the requested manga.
// ZeroedIndex is the chapter index preceded with zeros, i.e. Chapter 1
// will look like "00001".
// Finished is the channel, which the monitor go-routine will use to respond to,
// with the zip file's name.
type request struct {
	W           http.ResponseWriter
	R           *http.Request
	Path        string
	Manga       string
	ZeroedIndex string
	Index       int
	Finished    chan<- string
}

// isLongTimeAgo() will return true, if the current manga list is older than the
// recheckDuration period. Otherwise false.
func isLongTimeAgo() bool {
	return time.Now().Sub(lastCheck) > recheckDuration
}

// getMangas() retrieves a new manga list if necessary (see isLongTimeAgo()) and
// returns a map containing all manga names as keys, and all url-paths as
// values. Use getMangaList() to retrieve a slice containing all names.
func getMangas() map[string]parser.MangaUrl {
	if isLongTimeAgo() {
		refresh()
	}
	return mangas
}

// getMangaList() retrieves a new manga list if necessary (see isLongTimeAgo())
// and returns a slice containing all manga names. Use getMangas() to retrieve a
// map containing names and url-paths.
func getMangaList() []string {
	if isLongTimeAgo() {
		refresh()
	}
	if len(mangaList) == 0 {
		mangaList = mangaParser.GetMangasList()
	}
	return mangaList
}

// refresh() retrieves a new manga list and sets lastCheck to current time.
func refresh() {
	mangaParser = parser.NewMangaParser(mangaUrl)
	mangaList = []string{}
	mangaMap, err := mangaParser.GetMangasAndShortUrls()
	if err != nil {
		logger.Println("ERR", "refresh", err)
		mangas = mangaMap
		return
	}
	lastCheck = time.Now()
}

// writeChaptersResponse() prints all objects, residing in the slice chapters to
// w. The chapters will be printed in JSON format and may look
// like:
//	[ { "i": 1, "d": "01.02.2000" },
//	{ "i": 2, "d": "02.03.2001" },
//	...
//	{ "i": 100, "d": "01.01.2010" } ]
func writeChaptersResponse(w io.Writer, chapters []parser.Chapter) {
	fmt.Fprint(w, "[ ")
	for i, ch := range chapters {
		if i == len(chapters)-1 {
			fmt.Fprintf(w, "{ \"index\": %v, \"date\": \"%v\" } ",
				ch.Name, ch.Date.Format(dateFormat))
		} else {
			fmt.Fprintf(w, "{ \"index\": %v, \"date\": \"%v\" },\n",
				ch.Name, ch.Date.Format(dateFormat))
		}
	}
	fmt.Fprint(w, "]")
}

// listHandler() handles http requests, the server is catching on the listUrl.
// It writes all manga names to w. Gzip encoding is used if the client accepts
// it.
func listHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/plain")

	mangas := getMangaList()

	// if client doesn't accept gzip encoding, send plain text
	if !strings.Contains(r.Header.Get("Accept-Encoding"), "gzip") {
		enc := json.NewEncoder(w)
		enc.Encode(mangas)
		return
	}

	w.Header().Set("Content-Encoding", "gzip")
	gz := gzip.NewWriter(w)
	defer gz.Close()
	enc := json.NewEncoder(gz)
	enc.Encode(mangas)
}

// chapterHandler() handles http requests, the server is catching on chapterUrl.
// All chapter of the requested manga will be printed to w in JSON format.
// Gzip encoding is used if the client accepts it.
func chapterHandler(w http.ResponseWriter, r *http.Request) {
	manga := r.URL.Path[len(chapterUrl):]
	manga = strings.Replace(manga, "\\", "/", -1)
	logger.Println("DEBUG", "manga", manga)
	ch, ok := getMangas()[manga]
	if !ok {
		http.Error(w, "404 manga not found", http.StatusNotFound)
		return
	}
	chapterParser := parser.NewChapterParser(ch, manga)
	chapters, err := chapterParser.GetChaptersAndShortUrls()
	if err != nil {
		chapters = make([]parser.Chapter, 0)
		http.Error(w, "500 url is not valid", http.StatusInternalServerError)
		logger.Println("ERR", "invalid url:", err)
		return
	}

	w.Header().Set("Content-Type", "text/plain")
	// if client doesn't accept gzip encoding, send plain text
	if !strings.Contains(r.Header.Get("Accept-Encoding"), "gzip") {
		writeChaptersResponse(w, chapters)
		return
	}
	w.Header().Set("Content-Encoding", "gzip")
	gz := gzip.NewWriter(w)
	defer gz.Close()
	writeChaptersResponse(gz, chapters)
}

// coverHandler() handles http requests, the server is catching on coverUrl.
// Writes a "redirect" to w, which leads to the original url.
func coverHandler(w http.ResponseWriter, r *http.Request) {
	manga := r.URL.Path[len(coverUrl):]
	manga = strings.Replace(manga, "\\", "/", -1)
	logger.Println("DEBUG", "manga", manga)
	ch, ok := getMangas()[manga]
	if !ok {
		http.Error(w, "404 manga not found", http.StatusNotFound)
		return
	}
	coverParser := parser.NewCoverParser(ch)
	coverUrl, err := coverParser.GetCoverUrl()
	if err != nil {
		http.Error(w, "500 url is not valid", http.StatusInternalServerError)
		logger.Println("ERR", "invalid url:", err)
		return
	}

	logger.Println("DEBUG", "redirecting to", string(coverUrl))
	http.Redirect(w, r, string(coverUrl), http.StatusFound)
}

// getZipHandler() handles http requests, the server is catching on getUrl.
// It creates a new request object that will be send to the monitor go-routine.
// The monitor will answer with a path to a zip file, as soon as it is
// available.
func getZipHandler(w http.ResponseWriter, r *http.Request, lock chan<- request) {
	defer func(w http.ResponseWriter) {
		if msg := recover(); msg != nil {
			http.Error(w, "500 internal server error", http.StatusInternalServerError)
			logger.Println("ERR", msg)
		}
	}(w)
	query := r.URL.Path[len(getUrl):]
	parts := strings.Split(query, "/")
	if len(parts) != 2 {
		http.NotFound(w, r)
		logger.Println("DEBUG", "urlSplit query ==", query)
		return
	}

	manga := parts[0]
	manga = strings.Replace(manga, "\\", "/", -1)
	chap := strings.Split(parts[1], ".")
	if len(chap) > 2 {
		http.NotFound(w, r)
		logger.Println("DEBUG", "chSplit chapId ==", parts[1])
		return
	}
	chapterIndex, err := strconv.Atoi(chap[0])
	if err != nil {
		http.NotFound(w, r)
		logger.Println("DEBUG", "chapterAtoi", err)
		return
	}
	zeroFilledChapterIndex, err := intToZeroString(chapterIndex, 5)
	if err != nil {
		http.NotFound(w, r)
		logger.Printf("DEBUG zeroString chapterIndex: %v, err: %v\n",
			strconv.Itoa(chapterIndex), err)
	}
	path := baseFolder + "/" + manga + "/" + string(zeroFilledChapterIndex)

	unlock := make(chan string)
	req := request{W: w, R: r, Path: path, Manga: manga,
		ZeroedIndex: string(zeroFilledChapterIndex),
		Index:       chapterIndex,
		Finished:    unlock}
	lock <- req
	zippath := <-unlock
	// If the monitor go-routine encountered an error, it is already
	// handled (appropiate server response). We have nothing to do, return.
	if zippath == "" {
		return
	}
	zippy, err := os.Open(zippath)
	if err != nil {
		panic(err)
	}
	defer zippy.Close()

	serve(w, r, zippy)
}

// monitor() returns a channel, which is used by the getZipHandler function to
// send it's requests to.
// It supervises all get-zip-file requests to avoid conflicts during the
// download and generation of files. All get-zip-file requests are enqueued in a
// broadcast list. As soon as a requested zip file is available, it's path is
// send to all pending getZipHandler()s
func monitor() chan<- request {
	updates := make(chan request)
	// The broadcast lists are managed by a map whith
	// "<manga name>:::<chapter>" as keys. The values are a slice of
	// channels, where we should send the zip file's path to.
	worker := make(map[string][]chan<- string)
	go func() {
		for {
			// Waiting for a new get-zip-file request. If this
			// manga/chapter combination is already processing, the
			// new request will be enqueued, otherwise a new
			// go-routine is startet, which processes the request
			// and signal it's own pending getZipHandler() and all
			// other pending handlers. "" is send, if an error
			// occured. All errors have to be handled here, since
			// the getZipHandler expects that.
			req := <-updates
			if broadcast, ok := worker[req.Manga+":::"+req.ZeroedIndex]; ok {
				worker[req.Manga+":::"+req.ZeroedIndex] = append(broadcast, req.Finished)
			} else {
				worker[req.Manga+":::"+req.ZeroedIndex] = make([]chan<- string, 0)
				go func() {
					path := req.Path
					w := req.W
					r := req.R
					manga := req.Manga
					chapterIndex := req.Index
					defer func(w http.ResponseWriter) {
						if msg := recover(); msg != nil {
							http.Error(w, "500 internal server error", http.StatusInternalServerError)
							returnRequest(worker, req, "")
						}
					}(w)
					if zippy, err, ok := openOnExist(path + "/chapter.zip"); ok {
						if err != nil {
							logger.Println("ERR", err)
							panic("ioError/zipFile " + err.Error())
						}
						logger.Println("REQ", "Zip file found:", zippy.Name())
						zippy.Close()
						returnRequest(worker, req, path+"/chapter.zip")
						return
					}

					urlManga, err := mangaParser.GetUrlSuffix(manga)
					if err != nil {
						http.NotFound(w, r)
						logger.Println("DEBUG", "MangaSuffix", err)
						returnRequest(worker, req, "")
						return
					}
					logger.Println("DEBUG", "MangaName", manga)
					chapterParser := parser.NewChapterParser(urlManga, manga)
					chapterParser.GetChaptersAndShortUrls()
					urlCh, err := chapterParser.GetUrl(parser.ChapterIndex(chapterIndex))
					if err != nil {
						http.NotFound(w, r)
						logger.Println("DEBUG", "ChapterUrl", err)
						returnRequest(worker, req, "")
						return
					}
					logger.Printf("DEBUG manga:%v, chapterIndex:%v, urlManga:%v, urlCh:%v\n",
						manga, strconv.Itoa(chapterIndex), urlManga, urlCh)

					siteParser := parser.NewSiteParser(urlCh)
					sitesTmp, err := siteParser.GetSitesShortUrls()
					if err != nil {
						logger.Println("ERR", err)
						panic("openSite " + err.Error())
					}

					http.StatusText(http.StatusAccepted)

					var sites = make([]parser.Site, 0, len(sitesTmp))
					for _, site := range sitesTmp {
						/* .jpg is an assumption, it may be some other extension.
						 * In this case we look for every imgUrl below (next for loop).
						 */
						zeroString, err := intToZeroString(int(site.Num), 5)
						if err != nil {
							logger.Println("DEBUG", "loop/zeroString", err)
							continue
						}
						// only the extension .jpg is taken
						// zeroString is the filename
						filepath := mangaio.Path("ext.jpg", path, zeroString)
						exists, err := mangaio.Exists(filepath)
						if err != nil {
							logger.Println("ERR", err)
							panic("ioError/exists " + err.Error())
						}
						if !exists {
							sites = append(sites, site)
						}
					}

					/* For every missing img, we look for
					 * imgUrls and download them.
					 */
					for _, site := range sites {
						siteNumber, err := intToZeroString(int(site.Num), 5)
						if err != nil {
							logger.Println("DEBUG", "site/zeroString", err)
							continue
						}
						imgParser := parser.NewImageParser(site.Url)
						imgUrl, err := imgParser.GetImageUrl()
						if err != nil {
							logger.Println("ERR", err)
							panic("imageUrl " + err.Error())
						}
						err = mangaio.Save(string(imgUrl), path, siteNumber)
						if err != nil {
							logger.Println("ERR", err)
							panic("ioError/save " + err.Error())
						}
						logger.Printf("NEW img: %v\n\turl: %v\n",
							mangaio.Path(string(imgUrl), path, siteNumber), imgUrl)
					}

					zippy, err := createZipFile(path)
					if err != nil {
						logger.Println("ERR", err)
						panic("ioError/zipCreate " + err.Error())
					}
					logger.Println("NEW zip:", zippy.Name())
					zippy.Close()

					// zip file created successfully, delete image files
					go deleteImages(path)

					returnRequest(worker, req, path+"/chapter.zip")
				}()
			}
		}
	}()
	return updates
}

// returnRequest() is used by the monitor go-routine. It sends the provided path
// to the req's own pending handler and to all channels residing in the
// appropriate broadcast list residing in worker. In the end, the broadcast list
// is deleted.
func returnRequest(worker map[string][]chan<- string, req request, path string) {
	//signal current request
	req.Finished <- path
	//signal all pending requests
	signal(worker[req.Manga+":::"+req.ZeroedIndex], path)
	//remove broadcast list
	delete(worker, req.Manga+":::"+req.ZeroedIndex)
}

// signal() sends path to all channels residing in receiver.
func signal(receiver []chan<- string, path string) {
	for _, rec := range receiver {
		rec <- path
	}
}

// deleteImages() deletes all files, but zip files, residing in the directory
// represented as path.
func deleteImages(path string) {
	err := filepath.Walk(path,
		func(filepath string, info os.FileInfo, e error) error {
			if e != nil {
				return e
			}
			if info.IsDir() {
				return nil
			}
			if !strings.Contains(info.Name(), ".zip") {
				logger.Println("REMOVE", filepath)
				err := os.Remove(filepath)
				return err
			}
			return nil
		},
	)
	if err != nil {
		logger.Println("ERR", err)
	}
}

// Sends file to the client.
func serve(w http.ResponseWriter, r *http.Request, file *os.File) {
	logger.Println("REQ", "Serve:", file.Name())
	http.ServeFile(w, r, file.Name())
}

// createZipFile() creates a new zip file in path, containing all images
// residing in path.
func createZipFile(path string) (*os.File, error) {
	buf := new(bytes.Buffer)
	writer := zip.NewWriter(buf)
	err := filepath.Walk(path, makeCompressFunc(writer))
	if err != nil {
		panic("ioError/fileWalk " + err.Error())
	}
	err = writer.Close()
	if err != nil {
		panic("zipClose " + err.Error())
	}
	zipPath := path + "/chapter.zip"
	err = ioutil.WriteFile(zipPath, buf.Bytes(), 0777)
	if err != nil {
		panic("ioError/writeZip " + err.Error())
	}
	return os.Open(path + "/chapter.zip")
}

// makeCompressFunc() returns a function that is used by createZipFile(). It
// adds and writes a new file to w.
func makeCompressFunc(w *zip.Writer) filepath.WalkFunc {
	count := 0
	return func(path string, info os.FileInfo, e error) error {
		if e != nil || info.IsDir() {
			return e
		}
		count++
		f, _ := w.Create(filepath.Base(path))
		img, err := ioutil.ReadFile(path)
		_, err = f.Write(img)
		if err != nil {
			logger.Println("zipWrite", err)
			return err
		}
		return e
	}
}

// openOnExist() returns a file object and true if the file exists and can be
// opened. The returned error should be checked, since it may not be nil.
//
// If the file does not exists, openOnExist() returns nil, nil, false.
//
// If an unknown error occures, it returns nil, !nil, false.
func openOnExist(path string) (*os.File, error, bool) {
	exists, err := mangaio.Exists(path)
	if err == nil {
		if exists {
			file, e := os.Open(path)
			return file, e, true
		} else {
			return nil, nil, false
		}
	}
	return nil, err, false
}

// intToZeroString() converts num to string, filling it with zeros. The string's
// length is equal to n. intToZeroString() returns "", !nil, if num doesn't fit
// into n digits.
func intToZeroString(num, n int) (mangaio.ZeroString, error) {
	str := []byte(strconv.Itoa(num))
	if n < len(str) {
		newErr := errors.New(
			fmt.Sprintf("Int %v converted to string does not fit "+
				"into %v digit(s).", num, n))
		return mangaio.ZeroString(""), newErr
	}
	for len(str) < n {
		str = append([]byte("0"), str...)
	}
	return mangaio.ZeroString(string(str)), nil
}

// reqCounter() returns a channel, where the make*Handler() functions write to.
// It increments a counter if a new request is accepted by the server (receives
// 1 via the returned channel). It decrements the counter if a request is
// finished (receives -1 via channel). It print a debug message to the logger if
// it receives 0 via channel.
func reqCounter() chan<- int {
	updates := make(chan int)
	var counter int
	go func() {
		for {
			req := <-updates
			switch req {
			case -1:
				counter--
			case 0:
				logger.Println("DEBUG", "Pending requests:",
					strconv.Itoa(counter))
			case 1:
				counter++
			}
		}
	}()
	return updates
}

// makeHandler() returns a new HandlerFunc, which can be used for all urls, but
// get-zip-file url.
func makeHandler(fn func(http.ResponseWriter, *http.Request),
	reqUpdates chan<- int) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		reqUpdates <- 1
		logger.Println("REQ", "process new request:", r.URL.Path)
		fn(w, r)
		reqUpdates <- -1
		logger.Println("REQ", "request processed:", r.URL.Path)
	}
}

// makeGetZipHandler() returns a new HandlerFunc, which can be used for the
// get-zip-file url.
func makeGetZipHandler(fn func(http.ResponseWriter, *http.Request, chan<- request),
	requests chan<- request, reqUpdates chan<- int) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		reqUpdates <- 1
		logger.Println("REQ", "process new request:", r.URL.Path)
		fn(w, r, requests)
		reqUpdates <- -1
		logger.Println("REQ", "request processed:", r.URL.Path)
	}
}

// init() sets the default values for the commandline options. See pkg flag.
func init() {
	flag.StringVar(&port, "port", "8080",
		"port on which the server will listen")
	flag.StringVar(&dst, "dst", defaultDst,
		"output folder, where to save images and zip files")
}

// main() initializes the server. This includes dealing with commandline
// options, retrieve a manga list, start
// the monitor which is dealing with concurrent get-zip-file requests and
// setting up handler functions that will listen to several urls.
func main() {
	flag.Parse()
	if dst == "" {
		baseFolder = "."
	} else {
		baseFolder = dst
	}
	if baseFolder == defaultDst {
		os.Mkdir(defaultDst, 0700)
	}
	baseInfo, err := os.Stat(baseFolder)
	if err != nil {
		fmt.Println(err)
		return
	}
	if !baseInfo.IsDir() {
		fmt.Printf("%v is not a directory\n", baseInfo.Name())
		return
	}
	// test for write permission
	tmpFile, err := ioutil.TempFile(baseFolder, "")
	if err != nil {
		fmt.Println(err)
		return
	}
	tmpName := tmpFile.Name()
	tmpFile.Close()
	os.Remove(tmpName)

	mangas, err = mangaParser.GetMangasAndShortUrls()
	if err != nil {
		fmt.Println("init", err)
	}

	requestCounter := reqCounter()
	requestReceiver := monitor()

	http.HandleFunc(debugUrl, func(w http.ResponseWriter, r *http.Request) {
		logger.Println("DEBUG:::")
		requestCounter <- 0
	})
	http.HandleFunc(listUrl, makeHandler(listHandler, requestCounter))
	http.HandleFunc(chapterUrl, makeHandler(chapterHandler, requestCounter))
	http.HandleFunc(coverUrl, makeHandler(coverHandler, requestCounter))
	http.HandleFunc(getUrl, makeGetZipHandler(getZipHandler, requestReceiver, requestCounter))

	err = http.ListenAndServe(":"+port, nil)
	if err != nil {
		fmt.Println(err)
	}
}
